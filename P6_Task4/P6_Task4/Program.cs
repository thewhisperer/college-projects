﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P6_Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, i;
            bool odd;

            Console.WriteLine("Введите факториал: ");
            n = int.Parse(Console.ReadLine());
            odd = (n % 2 == 1) ? true : false;

            if (odd)
            {
                i = 3;
            }
            else
            {
                i = 2;
            }

            do
            {
                n *= i;
                i += 2;
            } while (i <= n);
            Console.WriteLine("Факториал равен: {0}", n);
            Console.ReadKey();
        }
    }
}
