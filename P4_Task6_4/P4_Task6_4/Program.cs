﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4_Task6_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, n = 1, f = 1;
            Console.WriteLine("a");
            a = int.Parse(Console.ReadLine());

            while (f > a || f * (n + 1) <= a) {
                n++;
                f *= n;
            }
            Console.WriteLine("Факториал: {0}", n);
            Console.ReadKey();
        }
    }
}
