﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P9_Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            Random rand = new Random();
            Console.WriteLine("Введите n: ");
            try
            {
                n = int.Parse(Console.ReadLine());
                int[] massiv = new int[n + 1];

                for (int i = 1; i <= n; i++)
                {
                    massiv[i] = rand.Next(-20, 20);
                    if (massiv[i] < 0)
                    {
                        massiv[i] *= massiv[i];
                    }
                }
                for (int i = 0; i < n; i++)
                {
                    massiv[i] = rand.Next(-20, 20);
                }
                Array.Sort(massiv);
                for (int i = 0; i < n; i++)
                {
                    massiv[i] = rand.Next(-20, 20);
                }
                foreach (int item in massiv) 
                {
                    Console.WriteLine(item);
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Ошибка формата");
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
