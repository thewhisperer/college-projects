﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P9_Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите размер: ");
            int n = int.Parse(Console.ReadLine());
            int[] massiv = new int[n + 1];
            Random rand = new Random();
            int temp;

            for (int i = 0; i <= n; i++) {
                massiv[i] = rand.Next(-100, 100);
            }

            for (int k = n - 1; k > 0; k--)
            {
                for (int i = 0; i <= k; i++)
                {
                    if (massiv[i] > massiv[i + 1])
                    {
                        temp = massiv[i];
                        massiv[i] = massiv[i + 1];
                        massiv[i + 1] = temp;
                    }
                }
            }
            foreach (int item in massiv)
            {
                Console.Write(item + " ");
            }
            Console.ReadKey();
        }
    }
}
