﻿using System;
using System.IO;

namespace P13_Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите N: ");
            int N = int.Parse(Console.ReadLine());
            double[] mas = new double[N];
            double[] x = new double[N];
            double y = 1;
            for (int i = 0; i < N; i++)
            {
                Console.WriteLine("Введите число, осталось {0}", N - i);
                mas[i] = double.Parse(Console.ReadLine());
            }
            BinaryWriter f = new BinaryWriter(new FileStream(@"C:\Users\win03\source\repos\P13_Task2\f", FileMode.Create));
            for (int i = 0; i < N; i++)
            {
                f.Write(mas[i]);
            }
            f.Close();
            BinaryReader fCon = new BinaryReader(new FileStream(@"C:\Users\win03\source\repos\P13_Task2\f", FileMode.Open));
            BinaryWriter g = new BinaryWriter(new FileStream(@"C:\Users\win03\source\repos\P13_Task2\g", FileMode.Create));
            try
            {
                int i = 0;
                while (true)
                {
                    x[i] = fCon.ReadDouble();
                    y *= x[i];
                    g.Write(y);
                    i++;
                }
            }
            catch (EndOfStreamException)
            {

            }
            finally
            {
                fCon.Close();
                g.Close();
                Console.WriteLine("Произведение: " + y);
                Console.WriteLine("Нажмите любую клавишу чтобы продолжить. . .");
                Console.ReadKey();
            }
        }
    }
}