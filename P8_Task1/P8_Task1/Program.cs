﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P8_Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            double u = 0, r = 0;

            Console.WriteLine("Введите напряжение: ");
            try
            {
                u = double.Parse(Console.ReadLine());
                Console.WriteLine("Введите сопротивление");
                r = double.Parse(Console.ReadLine());
            }
            catch (FormatException)
            { 
                Console.Title = "Ошибка: Неверный формат";
                Console.WriteLine("Неверный формат ввода");
            }
            Console.WriteLine("Сила тока: {0}", (u / r));
            Console.ReadKey();
        }
    }
}
