﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P6_Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, y, r, i = 1;

            Console.WriteLine("Введите радиус (R): ");
            r = int.Parse(Console.ReadLine());

            do
            {
                Console.WriteLine("Введите X: ");
                x = int.Parse(Console.ReadLine());
                Console.WriteLine("Введите Y:");
                y = int.Parse(Console.ReadLine());

                if (((x + r) * (x + r)) + ((y - r) * (y - r)) <= (r * r)) Console.WriteLine("Попал");
                else Console.WriteLine("Промах");

                i++;
            } while (i <= 10);
            Console.WriteLine("Нажмите любую клавишу чтобы продолжить. . .");
            Console.ReadKey();
        }
    }
}
