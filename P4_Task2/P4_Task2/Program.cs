﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4_Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            const double TAXMINUTES = 370;
            const double TAX = 135;
            const double OVERTAX = 2.5;

            double time = double.Parse(Console.ReadLine());

            Console.WriteLine((time > TAXMINUTES) ? TAX + ((time - TAXMINUTES) * OVERTAX) : TAX);
            Console.ReadKey();
        }
    }
}
