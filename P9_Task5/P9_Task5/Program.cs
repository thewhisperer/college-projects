﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P9_Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, max = -1, indexMax = -1, min = -1, indexMin = -1;
            Console.WriteLine("Введите размер массива: ");
            n = int.Parse(Console.ReadLine());
            int[] massiv = new int[n + 1];
            Random rand = new Random();

            for (int i = 0; i <= n; i++)
            {
                massiv[i] = rand.Next(-10, 10);
            }
            Console.WriteLine("Исходный массив: ");
            foreach (int item in massiv)
            {
                Console.Write(item + " ");
            }

            for (int i = 0; i < massiv.Length; i++)
            {
                if (max <= massiv[i])
                {
                    max = massiv[i];
                    indexMax = i;
                }
            }

            for (int i = 0; i < massiv.Length; i++)
            {
                if (min >= massiv[i])
                {
                    min = massiv[i];
                    indexMin = i;
                }
            }

            int temp = massiv[indexMin];
            massiv[indexMin] = massiv[indexMax];
            massiv[indexMax] = temp;
            Console.WriteLine();
            Console.WriteLine("Готовый массив: ");
            foreach (int item in massiv)
            {
                Console.Write(item + " ");
            }
            Console.ReadKey();
        }
    }
}
