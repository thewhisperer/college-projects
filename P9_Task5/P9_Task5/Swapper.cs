﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P9_Task5
{
    class Swapper
    {
        public void swap(int a, int b)
        {
           int temp = a;
            a = b;
            b = temp;
        }
    }
}
