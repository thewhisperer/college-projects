﻿using System;
using System.Windows.Forms;

namespace MatrixDeterminantCalculator
{
    public partial class FormSingle : Form
    {
        public FormSingle()
        {
            InitializeComponent();
        }
        /*
         * Событие клика кнопки, здесь происходит вся программа
         * @Author TheWhisperer(Булат Кадыков)
         */
        private void button1_Click(object sender, EventArgs e)
        {
            // Лестница инициализаций(Длина строки прибавляется по единице)
            int[] MatrixUp = new int[3];
            int[] MatrixMid = new int[3];
            int[] MatrixDown = new int[3];

            int Det;

            try
            {
                MatrixUp[0] = int.Parse(textBox1.Text);
                MatrixUp[1] = int.Parse(textBox2.Text);
                MatrixUp[2] = int.Parse(textBox3.Text);

                MatrixMid[0] = int.Parse(textBox4.Text);
                MatrixMid[1] = int.Parse(textBox5.Text);
                MatrixMid[2] = int.Parse(textBox6.Text);

                MatrixDown[0] = int.Parse(textBox7.Text);
                MatrixDown[1] = int.Parse(textBox8.Text);
                MatrixDown[2] = int.Parse(textBox9.Text);
            }
            catch (FormatException)
            {
                label1.Text = "Ошибка: Неправильный формат";
            }

            //Формула вычисления определителя(детерминанта) матрицы
            Det = MatrixUp[0] * MatrixMid[1] * MatrixDown[2] + MatrixUp[1] * MatrixMid[2] * MatrixDown[0] + MatrixUp[2] * MatrixMid[0] * MatrixDown[1] - MatrixUp[2] * MatrixMid[1] * MatrixDown[0] - MatrixMid[0] * MatrixUp[1] * MatrixDown[2] - MatrixMid[2] * MatrixDown[1] * MatrixUp[0];

            label1.Text = ("Результат: " + Det);
        }
    }
}
