﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P6_Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, x, s = 0;
            int n, i = 1;

            Console.WriteLine("Введите x: ");
            x = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите n: ");
            n = int.Parse(Console.ReadLine());
            a = Math.Sin(x);

            do
            {
                s += a;
                a = Math.Sin(a);
                i++;
            } while (i <= n);

            Console.WriteLine("Результат: {0}", s);
            Console.WriteLine("Нажмите любую клавишу чтобы продолжить. . .");
            Console.ReadKey();
        }
    }
}
