﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P6_Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int input, a, b, c, max = 0, x = 100, sum, result;
            
            input = int.Parse(Console.ReadLine()); //231
            if (input < 100 && input > 999)
            {
                Console.WriteLine("Можно ввести только 3-х значное число");
            }
            else
            {
                do
                {
                    a = input % 10; //1
                    b = ((input - a) / 10) % 10; //3
                    c = (((input - a) / 10) - b) / 10; //2
                    sum = a + b + c;
                    result = input / sum;

                    if (max < result)
                    {
                        max = result;
                    }

                    x++;
                } while (x <= 999);
                Console.WriteLine("Ответ: {0}", max);
            }
            Console.WriteLine("Нажмите любую клавишу чтобы продолжить. . .");
            Console.ReadKey();
        }
    }
}
