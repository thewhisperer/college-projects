﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4_Task6_3
{
    class Program
    {
        static void Main(string[] args)
        {
            sbyte i;
            double s;
            Console.WriteLine();
            double a = double.Parse(Console.ReadLine());

            if (a < 0)
            {
                s = 0;
                i = 3;
                while (i <= 9)
                {
                    s += (i - 2);
                    i += 2;
                }
            }
            else
            {
                s = 1;
                i = 2;
                while (i <= 8)
                {
                    s *= ((i * i) - a);
                    i += 2;
                }
            }
            Console.WriteLine(s);
            Console.ReadKey();
        }
    }
}
