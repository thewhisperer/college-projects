﻿using System;
using System.IO;

namespace P13_Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите N: ");
            int N = int.Parse(Console.ReadLine());
            Random hopanaRandom = new Random();
            BinaryWriter f = new BinaryWriter(new FileStream(@"C:\Users\win03\source\repos\P13_Task3\f", FileMode.Create));
            for (int i = 0; i < N; i++)
            { int y = hopanaRandom.Next(1, 50);
                f.Write(y);
                Console.Write(y + " ");
            }
            f.Close();
            BinaryReader fCon = new BinaryReader(new FileStream(@"C:\Users\win03\source\repos\P13_Task3\f", FileMode.Open));
            BinaryWriter g = new BinaryWriter(new FileStream(@"C:\Users\win03\source\repos\P13_Task3\g", FileMode.Create));
            int min = 501;
            int max = 0;
            try
            {
                while (fCon.PeekChar() > -1)
                {
                    int x = fCon.ReadInt32();
                    Console.Write(x+" ");
                    if (x < min)
                    {
                        min = x;
                    }
                    if (x > max)
                    {
                        max = x;
                    }
                }
                g.Write(min + max);
            }
            catch (EndOfStreamException)
            {

            }
            finally
            {
                fCon.Close();
                g.Close();
                Console.Write("Минимум {0}; Максимум {1}", min, max);
                Console.WriteLine("Summ");
                Console.WriteLine(min + max);
                Console.WriteLine("Нажмите любую клавишу чтобы продолжить. . .");
                Console.ReadKey();
            }
        }
    }
}
