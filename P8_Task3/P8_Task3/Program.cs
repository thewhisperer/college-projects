﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P8_Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            double dollarKurs = 0, rubKurs = 0;
            int dollar = 0, rub = 0;
            sbyte mode;

            Console.WriteLine("Введите режим(1 - перевод в рубли, 2 - перевод в доллары): ");
            try
            {
                mode = sbyte.Parse(Console.ReadLine());
                Console.WriteLine("Введите курс доллара: ");
                dollarKurs = double.Parse(Console.ReadLine());
                Console.WriteLine("Введите курс рубля: ");
                rubKurs = double.Parse(Console.ReadLine());

                switch (mode)
                {
                    case 1:
                        Console.WriteLine("Введите кол-во долларов: ");
                        dollar = int.Parse(Console.ReadLine());
                        Console.WriteLine("Результат: {0} руб.", (dollar * rubKurs));
                        break;
                    case 2:
                        Console.WriteLine("Введите кол-во рублей: ");
                        rub = int.Parse(Console.ReadLine());
                        Console.WriteLine("Результат: {0} $", (rub / dollarKurs));
                        break;
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Ошибка формата");
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Ошибка деления на 0");
            }
            Console.ReadKey();
        }
    }
}
