﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4_Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите код города");
            int code = int.Parse(Console.ReadLine());
            float price;

            switch (code)
            {
                case 095:
                    price = 10;
                    break;
                case 812:
                    price = 10;
                    break;
                case 0163:
                    price = 25.4f;
                    break;
                case 4242:
                    price = 41.5f;
                    break;
                default: 
                    price = 0;
                    break;
            }
            if (price != 0)
            {
                Console.WriteLine("Введите время: ");
                float total = (float.Parse(Console.ReadLine())) * price;
                Console.WriteLine("Итоговая стоимость: {0} руб.", total);
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Соединение не установлено");
                Console.ReadKey();
            }
        }
    }
}
