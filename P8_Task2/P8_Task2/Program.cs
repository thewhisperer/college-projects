﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P8_Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int u = 0, r = 0, i = 0;
            Console.Title = "Вычисление силы тока";

            Console.WriteLine("Введите напряжение: ");
            try
            {
                u = int.Parse(Console.ReadLine());
                Console.WriteLine("Введите сопротивление");
                r = int.Parse(Console.ReadLine());
                i = (u / r);
            }
            catch (FormatException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Title = "Ошибка: Неверный формат";
                Console.WriteLine("Ошибка: Неверный формат ввода");
                Console.WriteLine("Нажмите любую клавишу чтобы продолжить. . .");
                Console.ReadKey();
            }
            catch (DivideByZeroException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Title = "Ошибка: Невозможно деление на ноль";
                Console.WriteLine("Ошибка: Невозможно деление на ноль");
                Console.WriteLine("Нажмите любую клавишу чтобы продолжить. . .");
                Console.ReadKey();
            }
            if (r != 0 && u != 0) {
            Console.WriteLine("Сила тока: {0}", i);
            Console.ReadKey();
            }
        }
    }
}
