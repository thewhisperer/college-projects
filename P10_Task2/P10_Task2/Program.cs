﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P10_Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n: ");
            int n = int.Parse(Console.ReadLine());
            int [,] matrix = new int[n, n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i % 2 == 0)
                    {
                        matrix[i, j] = j + i;
                    }
                    else
                    {
                        matrix[i, j] = n - j;
                    }
                    Console.Write(matrix[i, j] + " ");
                    
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
