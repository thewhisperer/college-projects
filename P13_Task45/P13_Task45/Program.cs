﻿using System;
using System.IO;

namespace P13_Task45
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите N: ");
            int N = int.Parse(Console.ReadLine());
            BinaryWriter f = new BinaryWriter(new FileStream(@"C:\Users\win03\source\repos\P13_Task45\f", FileMode.Create));
            Random rand = new Random();
            for (int i = 0; i < N;i++)
            {
                f.Write(rand.Next(1, 10000));
            }
            f.Close();
            BinaryReader fCon = new BinaryReader(new FileStream(@"C:\Users\win03\source\repos\P13_Task45\f", FileMode.Open));
            BinaryWriter g = new BinaryWriter(new FileStream(@"C:\Users\win03\source\repos\P13_Task45\g", FileMode.Create));
            try
            {
                while (true)
                {
                    int x = fCon.ReadInt32();
                    if(x % 2 == 0)
                    {
                        g.Write(x);
                    }
                }
            } catch (EndOfStreamException)
            {

            }
            finally
            {
                fCon.Close();
                g.Close();
                Console.ReadKey();
            }
        }
    }
}
