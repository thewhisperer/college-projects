﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P7_Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, sum = 0, a = 1;
            Console.WriteLine("Сколько суммировать?");
            n = int.Parse(Console.ReadLine());
            for (int i = 0; i <= (n - 1); i++)
            {
                sum += a;
                a += 2;
            }
            Console.WriteLine("Ответ: {0}", sum);
            Console.ReadKey();
        }
    }
}
