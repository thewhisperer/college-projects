﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4_Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            sbyte type = 0;
            int price;
            double litrs = 0, total, exchange = 0, money = 0;
            const sbyte DISCOUNT = 5;

            Console.WriteLine("Выберите тип топлива: ");
            try
            {
                type = sbyte.Parse(Console.ReadLine());
            }
            catch (FormatException e)
            {
                Console.WriteLine("Неверный формат");
            }

            switch (type)
            {
                case 1:
                    price = 5;
                    break;
                case 2:
                    price = 10;
                    break;
                case 3:
                    price = 15;
                    break;
                case 4:
                    price = 20;
                    break;
                default:
                    price = 0;
                    break;
            }
            if (price == 0)
            {
                Console.WriteLine("Такого типа топлива нет");
            }
            else
            {
                Console.WriteLine("Введите количества топлива(в литрах): ");
                try
                {
                    litrs = int.Parse(Console.ReadLine());        
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Неверный формат");
                }

                total = litrs * price;
                if (litrs % 25 >= 1 || litrs == 25)
                {
                    double discount = total * DISCOUNT / 100;
                    total -= discount;
                }
                
                Console.WriteLine("Стоимость {0} руб.", total);
                Console.WriteLine("Деньги: ");
                try
                {
                    money = double.Parse(Console.ReadLine());
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Неверный формат");
                }

                exchange = money - total;

                if (exchange <= -1)
                {
                    Console.WriteLine("У вас не хватает средств");
                }
                else
                {
                    if (exchange == 0)
                    {
                        Console.WriteLine("Без сдачи");
                    }
                    else
                    {
                        Console.WriteLine("Сдача {0}", exchange);
                    }
                }
                Console.WriteLine("Нажмите любую клавишу чтобы продолжить. . .");
                Console.ReadKey();
            }
        }
    }
}
