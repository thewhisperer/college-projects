﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P7_Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            double y = 0;
            
            Console.WriteLine("Введите n: ");
            n = int.Parse(Console.ReadLine());
            double a = 2 * n + 1;

            for (int k = 2 * n + 1; k >= 1; k -= 2)
            {
                y += Math.Sqrt(a);
                a -= -2;
            }
            y = Math.Sqrt(y);
            Console.WriteLine(y);
            Console.ReadKey();

        }
    }
}
