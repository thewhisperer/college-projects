﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4_Task6_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int pass = 1234;
            int attempt;

            while (true)
            {
                Console.WriteLine("Введите пароль: ");
                attempt = int.Parse(Console.ReadLine());

                if (attempt == pass)
                {
                    Console.WriteLine("Пароль верный");
                    break;
                }
                else
                {
                    Console.WriteLine("Пароль не верный");
                }
            }
            Console.WriteLine("Нажмите любую кнопку чтобы продолжить. . .");
            Console.ReadKey();
            
        }
    }
}
