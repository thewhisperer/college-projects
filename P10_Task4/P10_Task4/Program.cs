﻿using System;
namespace P10_Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n: ");
            int n = int.Parse(Console.ReadLine());
            string[,] matrix = new string[n, n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if ((i > j && j < n / 2 && i + j < n - 1) || (i < j && j > n / 2 && i + j > n - 1))
                    {
                        matrix[i, j] = "=";
                    }
                    else
                    {
                        matrix[i, j] = "#";
                    }
                }
            }
            for ( int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i % 2 == 0)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                    } else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                    }
                    Console.Write(matrix[i, j]);
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
