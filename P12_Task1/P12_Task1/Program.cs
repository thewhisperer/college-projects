﻿using System;
using System.Text;

namespace P12_Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder builder = new StringBuilder("Первый Второй Третий");
            Console.WriteLine("Текст без изменений: " + builder.ToString());
            builder.Append(" AppendedString");
            Console.WriteLine("Текст с добавлением: " + builder.ToString());
            builder.Replace("AppendedString", " ReplacedString");
            Console.WriteLine("Текст с заменой: " + builder.ToString());
            builder.Remove(20, 16);
            Console.WriteLine("Текст с удалением: " + builder.ToString());
            builder.Insert(0, "InsertedString ");
            Console.WriteLine("Текст с вставкой: " + builder.ToString());
            Console.WriteLine("Нажмите любую клавишу чтобы продолжить. . .");
            Console.ReadKey();
        }
    }
}
