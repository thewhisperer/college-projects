﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4_Task5_2
{
    class Program
    {
        static void Main(string[] args)
        {
            sbyte sort = 0;
            int price = 0, items = 0, money = 0, total = 0, exchange = 0, i = 0;

            Console.WriteLine("Сколько хотите взять мороженных?");
            try
            {
                items = int.Parse(Console.ReadLine());
            }
            catch(FormatException e)
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.WriteLine(Settings.WRONGFORMAT);
                Console.Beep();
            }

            int[] weight = new int[items];
            items--;

            while (i <= items)
            {
                Console.WriteLine("Порция (грамм): ");
                try
                {
                    weight[i] = int.Parse(Console.ReadLine());
                }
                catch (FormatException e)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(Settings.WRONGFORMAT);
                    Console.Beep();
                    break;
                }

                Console.WriteLine("Выберите сорт(1 - 3): ");
                try
                {
                    sort = sbyte.Parse(Console.ReadLine());
                }
                catch (FormatException e)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(Settings.WRONGFORMAT);
                    Console.Beep();
                    break;
                }

                switch (sort)
                {
                    case 1:
                        price = 5;
                        break;
                    case 2:
                        price = 10;
                        break;
                    case 3:
                        price = 15;
                        break;
                    default:
                        price = 0;
                        break;
                }
                if (price == 0)
                {
                    Console.WriteLine("Такого сорта нет");
                    break;
                }
                else
                {
                    total += price * weight[i];
                    i++;
                }
            }

            if (price != 0)
            {
                Console.WriteLine("Стоимость: {0} руб.", total);
                Console.WriteLine("Деньги: ");
                try
                {
                    money = int.Parse(Console.ReadLine());
                }
                catch (FormatException e)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(Settings.WRONGFORMAT);
                    Console.Beep();
                }

                exchange = money - total;

                if (exchange <= -1 && money != 0)
                {
                    Console.WriteLine("У вас не хватает {0} руб.", Math.Abs(exchange));
                }
                else if (money != 0)
                {
                    Console.WriteLine("Сдача: {0}", exchange);
                }
            }

            Console.WriteLine("Нажмите любую клавишу чтобы продолжить. . .");
            Console.ReadKey();
        }
    }
}
